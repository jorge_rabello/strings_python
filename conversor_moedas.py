class CurrencyConverter:
    def __init__(self, quantidade, moeda_origem, moeda_destino):
        self.valor_dolar = 5.46
        self.quantidade = quantidade
        self.moeda_origem = moeda_origem
        self.moeda_destino = moeda_destino

    def converter(self):
        self.valida_quantidade(self.quantidade)
        if self.moeda_origem == 'BRL' and self.moeda_destino == 'USD':
            return self.converte_brl_para_usd()
        elif self.moeda_origem == 'USD' and self.moeda_destino == 'BRL':
            return self.converte_usd_para_brl()
        else:
            raise ValueError('Tipo de moeda inválida')

    def converte_usd_para_brl(self):
        print(round(self.quantidade * self.valor_dolar, 2))

    def converte_brl_para_usd(self):
        print(round(self.quantidade / self.valor_dolar, 2))

    def valida_quantidade(self, quantidade):
        if quantidade < 1:
            raise ValueError(
                'Não é possível converter valores negativos ou zeros')
