import re

endereco = "Rua das Flores, 72, apartamento 1002, Laranjeiras, Rio de Janeiro, RJ, 23440-120"

# regex pra cep
# 5 dígitos + hífen opcional + 3 dígitos
# qualquer número de zero a nove é válido e espera-se 5 blocos de números que podem variar entre 0 a 9 [0-9]{5}
# espera-se um hífen que pode ocorrer 0 ou 1 vez [-]{0,1}
# qualquer número de zero a nove é válido e espera-se 3 blocos de números que podem variar entre 0 a 9 [0-9]{3}
padrao = re.compile("[0-9]{5}[-]{0,1}[0-9]{3}")

busca = padrao.search(endereco)  # Match

if busca:
    cep = busca.group()
    print(cep)

