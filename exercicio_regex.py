import re

dados_pessoais = "Rafaela Brasil, CPF: 718.457.190-85"

padrao = re.compile("[0-9]{3}[.]{0,1}[0-9]{3}[.]{0,1}[0-9]{3}[-]{0,1}[0-9]{2}")

busca = padrao.search(dados_pessoais)

if busca:
    cpf = busca.group()
    print(cpf)
