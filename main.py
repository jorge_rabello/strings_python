from conversor_moedas import CurrencyConverter
from url_extractor import URLExtractor


def main():
    url_extractor = URLExtractor(
        "https://bytebank.com/cambio?moedaDestino=BRL&quantidade=1&moedaOrigem=USD")

    moeda_origem = url_extractor.get_valor_parametro("moedaOrigem").upper()
    moeda_destino = url_extractor.get_valor_parametro("moedaDestino").upper()
    quantidade = float(url_extractor.get_valor_parametro("quantidade"))
    curency_converter = CurrencyConverter(
        quantidade, moeda_origem, moeda_destino)

    curency_converter.converter()


if __name__ == "__main__":
    main()
